# Pixan/Users #

Pixan Users es el Api para gestionar los usuarios de nuestra aplicacíon y el reseteo de su password, ademas incorpora FacebookAuth para la autenticación con FB.


## UsersController ##

El paquete provee el "UsersController"  con el "Users" Trait, el cual expone los métodos más comunes para la gestión de usuarios.

## Users routes ##

Es necesario agregar las siguientes rutas al proyecto para hacer uso de los  métodos básicos del **UsersController**: store, show, update, delete, destroy, deactivate, activate, reset-password, reset
```
#!php

Route::group(['prefix' => 'api'], function(){
	Route::group(['prefix' => 'v1'], function(){

        Route::post('users', 'Api\v1\UsersController@store')->name('api.users.store');
        Route::get('users/{id}', 'Api\v1\UsersController@show')->name('api.users.show');
        Route::put('users/{id}', 'Api\v1\UsersController@update')->name('api.users.update');
        Route::delete('users/{id}', 'Api\v1\UsersController@delete')->name('api.users.delete');
        Route::delete('users/{id}/destroy', 'Api\v1\UsersController@destroy')->name('api.users.destroy');
        Route::put('users/{id}/deactivate', 'Api\v1\UsersController@deactivate')->name('api.users.deactivate');
		Route::put('users/{id}/activate', 'Api\v1\UsersController@activate')->name('api.users.activate');
		Route::post('users/reset-password', 'Api\v1\UsersController@resetPass')->name('api.users.resetPassword');
		Route::get('password/reset/{token?}','Api\v1\UsersController@showResetForm')->name('api.users.showResetForm');
		Route::post('password/reset', 'Api\v1\UsersController@postReset')->name('api.users.reset');

	});
});


```


## Instalación ##
Agregar en composer.json **"pixan/users": "dev-master"**

```
#!json
{
    "require": {
		"laravel/framework": "5.0.*",
		"pixan/users": "dev-master"
    }
}
```

Ejecutar composer update para descargar el paquete

```
#!shell

composer update
```

En el archivo **config/app.php** en el array de **providers** agregar


```
#!php

'Pixan\Users\UsersServiceProvider'

```

Publicar la **configuración** del paquete

```
#!shell

php artisan vendor:publish
```

Correr las **migraciones**


```
#!shell

php artisan migrate
```

Cambiar el valor de la posición 'passwords.users.email' en el archivo **config/auth.php** por 'pixan.users.password-email'


```
#!shell

'passwords' => [
	'users' => [
		'provider' => 'users',
		'email' => 'pixan.users.password-email',
		'table' => 'password_resets',
		'expire' => 60,
	],
],
```
