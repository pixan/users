<?php

namespace Pixan\Users;

use Illuminate\Support\ServiceProvider;

class UsersServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'pixan/users');


        $this->publishes([
			__DIR__.'/migrations/2016_10_26_193500_add_fb_columns_to_users_table.php' => 'database/migrations/2016_10_26_193500_add_fb_columns_to_users_table.php',
            __DIR__.'/views' => resource_path('views/pixan/users'),
            __DIR__ . '/config/pixanusers.php' => config_path('pixanusers.php'),
            __DIR__ . '/controllers/UsersController.php' => 'app/Http/Controllers/Api/v1/UsersController.php'
        ]);


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/traits/Users.php';

        $this->mergeConfigFrom(
            __DIR__.'/config/pixanusers.php', 'pixanusers'
        );
    }

}
