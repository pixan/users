<?php
namespace Pixan\Users\Transformers;

use Pixan\Api\Transformers\Transformer;

class UserTransformer extends Transformer {


	public function transform($user){
		$transformation = [
			'id' 				=> intval($user["id"]),
			'name' 				=> $user["name"],
			'email' 			=> $user["email"],
		];

		if(isset($user['profilePicture'])){
			// TODO
			// Verificar si la imagen existe, o regresar un avatar por default
			$transformation['profile_picture'] = route('api.v1.media.show', [$user['profilePicture']['filename']]);

		}

		return $transformation;
	}

}
