<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFbColumnsToUsersTable extends Migration
{

    public function up(){
        Schema::table('users', function ($table) {
            $table->string('facebook_id')->after('email')->nullable();
            $table->string('mobile_phone')->after('facebook_id')->nullable();
            $table->date('date_of_birth')->after('mobile_phone')->nullable();
            $table->integer('profile_picture_id')->after('date_of_birth')->nullable();
            $table->boolean('active')->after('profile_picture_id');
            $table->softDeletes();
        });
    }

    public function down(){
        Schema::table('users', function ($table) {
            $table->dropColumn('facebook_id');
            $table->dropColumn('mobile_phone');
            $table->dropColumn('date_of_birth');
            $table->dropColumn('profile_picture_id');
            $table->dropColumn('active');
            $table->dropSoftDeletes();
        });
    }

}
