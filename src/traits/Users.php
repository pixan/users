<?php

namespace Pixan\Users\Traits;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Pixan\Users\Models\User;
use Validator;
use Response;
use Pixan\Api\Controllers\ApiController;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Facebook\FacebookResponseException;
use Pixan\Users\Transformers\UserTransformer;

trait Users
{
    use ResetsPasswords;

    protected $apiController,$user, $fb;
    public $resetView = 'pixan.users.password-reset';
    public $resetConfirmView = 'pixan.users.password-reset-confirm';

 	public function __construct(ApiController $apiController, User $user, UserTransformer $userTransformer){
 		$this->apiController = $apiController;
        $this->user = $user;
        $this->userTransformer = $userTransformer;
 	}

    public function store(Request $request, LaravelFacebookSdk $fb)
    {
        $validator = Validator::make($request->all(), [
            'facebook_id' => 'numeric',
            'name' => 'string',
            'email' => 'email|unique:users,email',
            'password' => 'alpha_num|between:8,12',
            'date_of_birth' => 'string|size:10',
            'profile_picture_id' => 'exists:media,id'
        ],[
			'name.required' => 'Debe proporcionar su nombre',
			'name.string' => 'El nombre proporcionado no es válido',
			'email.unique' => 'La cuenta de correo electrónico proporcionada ya está registrada',
			'email.required' => 'Debe proporcionar una cuenta de correo electrónico',
			'email.email' => 'La cuenta de correo electrónico proporcionada no es válida',
			'facebook_id.numeric' => 'El identificador de Facebook ID debe ser un número',
	        'password.required' => 'Debe proporcionar su contraseña',
            'password.alpha_num' => 'La contraseña proporcionada debe ser alfanumerica',
            'password.between' => 'La contraseña proporcionada debe tener mínimo 8 y máximo 12 caracteres'
		]);

        $validator->sometimes('name', 'required|string', function($input){
			return !$input->offsetExists('facebook_id');
		});
		$validator->sometimes('email', 'required|email|unique:users,email', function($input){
			return !$input->offsetExists('facebook_id');
		});
		$validator->sometimes('password', 'required|alpha_num|between:8,12', function($input){
			return !$input->offsetExists('facebook_id');
		});


        if ($validator->fails()) {
            return $this->apiController->respondWithValidationErrors($validator->errors());
        }

        if($request->has('facebook_id') && $request->has('facebook_access_token')){

			try {

				$fb_response = $fb->get('/me?fields=id,name,email,birthday', $request->get('facebook_access_token'));
				if($fb_response->getGraphUser()->getId() != $request->get('facebook_id')){
					// Facebook ID proporcionado es incorrecto, no concuerda con
					// la respuesta de Facebook para ese Access Token
					return $this->apiController->respondWithErrors(['El usuario para el access token proporcionado no corresponde a ese Facebook ID']);
				}

				$validator = Validator::make([
					'email' => $fb_response->getGraphUser()->getEmail()
				], [
					'email' => 'email|unique:users,email',
		        ], [
					'email.unique' => 'Ya tiene una cuenta registrada, use estos datos para iniciar sesión'
				]);
				if($validator->fails()){
					return $this->apiController->respondWithValidationErrors($validator->messages()->all());
				}
				$user = User::create([
					'name' => $fb_response->getGraphUser()->getName(),
					'email' => $fb_response->getGraphUser()->getEmail(),
					'facebook_id' => $fb_response->getGraphUser()->getId(),
					'password' => $request->get('facebook_access_token'),
                    'active' => config('pixanusers.USER_ACTIVE')
				]);
				// echo $request->getGraphUser()->getName();
				// echo $request->getGraphUser()->getId();
				// echo $request->getGraphUser()->getEmail();
				// echo $request->getGraphUser()->getBirthday()->format('Y-m-d');
			} catch (\Facebook\Exceptions\FacebookSDKException $e) {
				// Failed to obtain access token
				return $this->apiController->respondWithErrors(['No fue posible validar el access token para la cuenta de Facebook']);
			}
		}

		if(!isset($user)){
            if($validator->fails()){
                return $this->apiController->respondWithValidationErrors($validator->messages()->all());
            }
            
            $data = $request->all();
            $data['active'] = config('pixanusers.USER_DEACTIVE');
			$user = User::create($data);
			$user->profilePicture;
		}

		return $this->apiController->setMessages(['El usuario fue registrado con éxito'])->respondCreated([
			'user' => $this->userTransformer->transform($user),
			'Authorization' => 'Basic '.base64_encode($user->email.':'.($request->has('facebook_access_token') ? $request->get('facebook_access_token') : $request->get('password'))),
		]);

    }

    public function show($id){
        $user = User::find($id);
        if($user)
            return $this->apiController->respondWithData($user);
        else
            return $this->apiController->respondNotFound(['Usuario no encontrado']);
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id
        ]);

        if ($validator->fails()) {
            return $this->apiController->respondWithValidationErrors($validator->errors());
        }

        $user = $this->user->find($id);
        if($user){
            $user->update($request->all());
            $this->apiController->setMessages(['Usuario actualizado exitosamente']);
            return $this->apiController->respondWithData($user);
        }else{
            return $this->apiController->respondNotFound(['Usuario no encontrado']);
        }

    }

    //destroy user, delete row in database
    public function destroy($id){
        $user = User::find($id);
        if($user){
            $user->forceDelete();
            $this->apiController->setMessages(['Cuenta de usuario eliminada exitosamente']);
            return $this->apiController->respondWithData($user);
        }else{
            return $this->apiController->respondNotFound(['Usuario no encontrado']);
        }
    }


    //delete user_account
    public function delete($id){
        $user = User::find($id);
        if($user){
            $user->delete();
            $this->apiController->setMessages(['Cuenta de usuario eliminada exitosamente']);
            return $this->apiController->respondWithData($user);
        }else{
            return $this->apiController->respondNotFound(['Usuario no encontrado']);
        }
    }

    //deactivate user_account
    public function deactivate($id){
        $user = User::find($id);
        if($user){
            $user->active = config('pixanusers.USER_DEACTIVE');
            $user->update();
            $this->apiController->setMessages(['Cuenta de usuario desactivada exitosamente']);
            return $this->apiController->respondWithData($user);
        }else{
            return $this->apiController->respondNotFound(['Usuario no encontrado']);
        }
    }

    //activate user_account
    public function activate($id){
        $user = User::find($id);
        if($user){
            $user->active = config('pixanusers.USER_ACTIVE');
            $user->update();
            $this->apiController->setMessages(['Cuenta de usuario activada exitosamente']);
            return $this->apiController->respondWithData($user);
        }else{
            return $this->apiController->respondNotFound(['Usuario no encontrado']);
        }
    }

    public function resetPass(Request $request){

        $this->validateSendResetLinkEmail($request);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink(
            $this->getSendResetLinkEmailCredentials($request),
            $this->resetEmailBuilder()
        );

        switch ($response) {
            case Password::RESET_LINK_SENT:
                $this->apiController->setStatusCode(200);
                $this->apiController->setMessages(['Correo electrónico enviado con exito']);
                return $this->apiController->respond();
            case Password::INVALID_USER:
                return $this->apiController->respondNotFound(['Usuario no valido']);
            default:
                return $this->apiController->respond();
        }
    }

    public function reset(Request $request)
    {
        $this->validate(
            $request,
            $this->getResetValidationRules(),
            $this->getResetValidationMessages(),
            $this->getResetValidationCustomAttributes()
        );

        $credentials = $this->getResetCredentials($request);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return view($this->resetConfirmView)->with(compact('user'));
            default:
                return view($this->resetConfirmView)->with(compact('user'));
        }
    }


}
